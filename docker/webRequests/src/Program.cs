﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace DockerSamples.DotNetConf2019
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("API calls started");

            await InitGetRequests();

            Console.WriteLine("API calls finished");
        }

        private static async Task InitGetRequests()
        {

            using (var httpClient = new HttpClient())
            {
                string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"urls.txt");

                var file = new StreamReader(path);
                string url = string.Empty;
                int counter = 0;

                while ((url = file.ReadLine()) != null)
                {
                    try
                    {
                        Console.WriteLine($" ========STARTED: {url}===========");
                        Console.WriteLine(await httpClient.GetAsync(url));
                        counter++;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }

                file.Dispose();
            }

        }
    }
}